﻿using System.Diagnostics;

namespace AngularGoogleMaps.Models
{
    [DebuggerDisplay("{Horizontal} {Vertical}")]
    public class Anchor
    {
        public AnchorHorizontal Horizontal { get; set; }

        public AnchorVertical Vertical { get; set; }

        public Anchor()
        {
            Horizontal = AngularGoogleMaps.Models.AnchorHorizontal.Style.Center;
            Vertical = AngularGoogleMaps.Models.AnchorVertical.Style.Bottom;
        }
    }
}
